import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[jhiHighlight]'
})
export class HighlightDirective {
  constructor(private el: ElementRef) {}

  /*    @HostListener('mouseenter') onMouseEnter(): void {
    this.mouseListen("table-primary");
   }

   @HostListener('mouseleave') onMouseLeave(): void {
    this.mouseListen(null)
   }

   mouseListen(param: any) : void {
    if(this.el) this.el.nativeElement.class = param;
   }  */

  @HostListener('mouseenter') onMouseEnter(): void {
    this.highlight('#b8daff', '#7abaff');
  }

  @HostListener('mouseleave') onMouseLeave(): void {
    this.highlight(null, null);
  }

  private highlight(color: any, color2: any): void {
    this.el.nativeElement.style.backgroundColor = color;
    this.el.nativeElement.style.borderColor = color2;
  }
}
