import { Directive, Input } from '@angular/core';
import { Validator, FormGroup, ValidationErrors, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[jhiMustMatch]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MustMatchDirective, multi: true }]
})
export class MustMatchDirective implements Validator {
  @Input('jhiMustMatch') values: string[] = [];

  constructor() {}

  validate(formGroup: FormGroup): ValidationErrors | null {
    if (this.values.length <= 1) return null;
    else return this.mustMatch(this.values[0], this.values[1])(formGroup);
  }

  mustMatch(controlName: string, matchingControlName: string): any {
    return (formGroup: FormGroup): any => {
      const control = formGroup.controls[controlName];
      const controlMatch = formGroup.controls[matchingControlName];

      if (!control || !controlMatch) {
        return null;
      }

      if (control.value !== controlMatch.value) {
        controlMatch.setErrors({ mustMatch: true });
        return controlMatch.getError('mustMatch');
      } else if (controlMatch.hasError('mustMatch')) {
        controlMatch.setErrors({ mustMatch: false });
        controlMatch.updateValueAndValidity();
        return null;
      }
    };
  }
}
