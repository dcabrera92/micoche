import { Directive, Input } from '@angular/core';
import { FormControl, ValidationErrors, Validator, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[jhiForbiddenName]',
  providers: [{ provide: NG_VALIDATORS, useExisting: ForbiddenNameDirective, multi: true }]
})
export class ForbiddenNameDirective implements Validator {
  @Input('jhiForbiddenName') name = '';

  constructor() {}

  validate(formControl: FormControl): ValidationErrors {
    return this.name && this.name !== '' ? this.forbiddenNameValidator(new RegExp(this.name, 'i'))(formControl) : null;
  }

  forbiddenNameValidator(nameReg: RegExp): any {
    return (control: FormControl) => {
      const forbidden = nameReg.test(control.value);
      return forbidden ? { forbiddenName: true } : null;
    };
  }
}
