import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightDirective } from './highlight.directive';
import { MustMatchDirective } from './must-match.directive';
import { ForbiddenNameDirective } from './forbidden-name.directive';

@NgModule({
  declarations: [HighlightDirective, MustMatchDirective, ForbiddenNameDirective],
  imports: [CommonModule],
  exports: [HighlightDirective, MustMatchDirective, ForbiddenNameDirective]
})
export class DirectivesModule {}
