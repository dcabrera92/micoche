export interface IPropietario {
  id?: number;
  nombre?: string;
  apellido?: string;
  dni?: string;
}

export class Propietario implements IPropietario {
  constructor(public id?: number, public nombre?: string, public apellido?: string, public dni?: string) {}
}
