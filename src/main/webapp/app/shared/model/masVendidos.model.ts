export interface ICocheV {
  marca?: string;
  vendido?: number;
}

export class CocheV implements ICocheV {
  constructor(public marca?: string, public vendido?: number) {}
}
