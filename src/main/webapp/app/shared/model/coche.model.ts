import { IIncidencia } from 'app/shared/model/incidencia.model';
import { IPropietario } from './propietario.model';

export interface ICoche {
  id?: number;
  marca?: string;
  modelo?: string;
  precio?: number;
  vendido?: boolean;
  matricula?: string;
  color?: string;
  incidencias?: IIncidencia[];
  propietario?: IPropietario;
  datesold?: Date;
}

export class Coche implements ICoche {
  constructor(
    public id?: number,
    public marca?: string,
    public modelo?: string,
    public precio?: number,
    public vendido?: boolean,
    public matricula?: string,
    public color?: string,
    public incidencias?: IIncidencia[],
    public propietario?: IPropietario,
    public datesold?: Date
  ) {
    this.vendido = this.vendido || false;
  }
}
