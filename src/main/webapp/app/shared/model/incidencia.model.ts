import { ICoche } from 'app/shared/model/coche.model';

export interface IIncidencia {
  id?: number;
  detalle?: string;
  estado?: boolean;
  coche?: ICoche;
}

export class Incidencia implements IIncidencia {
  constructor(public id?: number, public detalle?: string, public estado?: boolean, public coche?: ICoche) {
    this.estado = this.estado || false;
  }
}
