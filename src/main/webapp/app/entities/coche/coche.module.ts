import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MicocheSharedModule } from 'app/shared/shared.module';
import { CocheComponent } from './coche.component';
import { CocheDetailComponent } from './coche-detail.component';
import { CocheUpdateComponent } from './coche-update.component';
import { CocheDeleteDialogComponent } from './coche-delete-dialog.component';

import { MiniJuegoComponent } from './mini-juego.component';
import { cocheRoute } from './coche.route';
import { CocheUpdateOwnerDialogComponent } from './coche-update-owner-dialog.component';

@NgModule({
  imports: [MicocheSharedModule, RouterModule.forChild(cocheRoute)],
  declarations: [
    CocheComponent,
    CocheDetailComponent,
    CocheUpdateComponent,
    CocheDeleteDialogComponent,
    MiniJuegoComponent,
    CocheUpdateOwnerDialogComponent
  ],
  entryComponents: [CocheDeleteDialogComponent, CocheUpdateOwnerDialogComponent]
})
export class MicocheCocheModule {}
