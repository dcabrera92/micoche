import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICoche } from 'app/shared/model/coche.model';
import { CocheService } from './coche.service';
import { IPropietario, Propietario } from '../../shared/model/propietario.model';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  templateUrl: './coche-update-owner-dialog.component.html'
})
export class CocheUpdateOwnerDialogComponent implements OnInit {
  coche!: ICoche;
  propietarios: IPropietario[] = [];
  isSaving = false;
  propietario?: IPropietario;

  buyCoche = this.fb.group({
    id: [],
    nombre: [null, [Validators.required]],
    apellido: [null, [Validators.required]],
    dni: [null, [Validators.required]],
    email: [null, [Validators.required]],
    confirmEmail: []
  });

  constructor(
    protected cocheService: CocheService,
    private fb: FormBuilder,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  ngOnInit(): void {
    if (!this.coche.propietario) this.coche.propietario = new Propietario();
  }

  subscribeToSaveResponse(result: Observable<HttpResponse<ICoche>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  previousState(): void {
    window.history.back();
  }

  onSaveSuccess(): void {
    this.isSaving = false;
  }

  onSaveError(): void {
    this.isSaving = false;
    alert('algo al fallado');
  }

  updateOwner(coche: ICoche): void {
    this.isSaving = true;

    coche.vendido = !coche.vendido;
    coche.datesold = new Date();
    if (!coche.vendido) {
      coche.propietario = undefined;
      coche.datesold = undefined;
    }
    if (!coche.propietario) {
      coche.datesold = undefined;
      coche.vendido = undefined;
    }
    this.subscribeToSaveResponse(this.cocheService.update(coche));
    this.activeModal.close();
  }
}
