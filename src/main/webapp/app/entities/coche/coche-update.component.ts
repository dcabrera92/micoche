import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICoche, Coche } from 'app/shared/model/coche.model';
import { CocheService } from './coche.service';
import { PropietarioService } from '../propietario/propietario.service';
import { map } from 'rxjs/operators';
import { IPropietario } from '../../shared/model/propietario.model';

@Component({
  selector: 'jhi-coche-update',
  templateUrl: './coche-update.component.html'
})
export class CocheUpdateComponent implements OnInit {
  isSaving = false;
  coche: ICoche[] = [];
  propietarios: IPropietario[] = [];

  editForm = this.fb.group({
    id: [],
    marca: [null, [Validators.required, Validators.maxLength(20)]],
    modelo: [null, [Validators.required, Validators.maxLength(20)]],
    precio: [null, [Validators.required]],
    vendido: [null, [Validators.required]],
    matricula: [null, [Validators.required, Validators.maxLength(15)]],
    color: [null, [Validators.required, Validators.maxLength(20)]],
    propietario: [],
    datesold: []
  });

  constructor(
    protected cocheService: CocheService,
    protected propietarioService: PropietarioService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coche }) => {
      this.coche = coche;
      this.updateForm(coche);

      this.propietarioService
        .query()
        .pipe(
          map((res: HttpResponse<IPropietario[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IPropietario[]) => (this.propietarios = resBody));
    });
  }

  updateForm(coche: ICoche): void {
    this.editForm.patchValue({
      id: coche.id,
      marca: coche.marca,
      modelo: coche.modelo,
      precio: coche.precio,
      vendido: coche.vendido,
      matricula: coche.matricula,
      color: coche.color,
      propietario: coche.propietario,
      datesold: coche.datesold
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const coche = this.createFromForm();

    if (!coche.vendido) {
      coche.propietario = undefined;
      coche.datesold = undefined;
    }
    if (coche.id !== undefined) {
      this.subscribeToSaveResponse(this.cocheService.update(coche));
    } else {
      this.subscribeToSaveResponse(this.cocheService.create(coche));
    }
  }

  private createFromForm(): ICoche {
    return {
      ...new Coche(),
      id: this.editForm.get(['id'])!.value,
      marca: this.editForm.get(['marca'])!.value,
      modelo: this.editForm.get(['modelo'])!.value,
      precio: this.editForm.get(['precio'])!.value,
      vendido: this.editForm.get(['vendido'])!.value,
      matricula: this.editForm.get(['matricula'])!.value,
      color: this.editForm.get(['color'])!.value,
      propietario: this.editForm.get(['propietario'])!.value,
      datesold: this.editForm.get(['datesold'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICoche>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
