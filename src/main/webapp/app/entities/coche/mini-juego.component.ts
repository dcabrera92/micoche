import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-mini-juego',
  templateUrl: './mini-juego.component.html'
})
export class MiniJuegoComponent implements OnInit, OnDestroy {
  eventSubscriber?: Subscription;
  piedra: String = '../../../content/images/piedra.jpg';
  papel: String = '../../../content/images/papel.jpg';
  tijera: String = '../../../content/images/tijera.jpg';
  imgCpu: String | undefined;
  ramdomSelect: String | undefined;
  resultado: any;

  constructor(protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  usuarioSelect(seleccion: String): any {
    const ramdom: Number = Math.random();

    if (ramdom < 0.34) {
      this.ramdomSelect = 'piedra';
      this.imgCpu = this.piedra;
    } else if (ramdom <= 0.67) {
      this.ramdomSelect = 'papel';
      this.imgCpu = this.papel;
    } else {
      this.ramdomSelect = 'tijera';
      this.imgCpu = this.tijera;
    }

    this.comparacion(seleccion, this.ramdomSelect);
  }

  comparacion(seleccion1: any, seleccion2: any): any {
    if (seleccion1 === seleccion2) {
      this.resultado = 'empate';
    } else {
      if (seleccion1 === 'piedra') {
        if (seleccion2 === 'tijera') {
          this.resultado = true;
        } else {
          this.resultado = false;
        }
      } else if (seleccion1 === 'papel') {
        if (seleccion2 === 'piedra') {
          this.resultado = true;
        } else {
          this.resultado = false;
        }
      } else {
        if (seleccion1 === 'tijera') {
          if (seleccion2 === 'piedra') {
            this.resultado = false;
          } else {
            this.resultado = true;
          }
        }
      }
    }
  }
}
