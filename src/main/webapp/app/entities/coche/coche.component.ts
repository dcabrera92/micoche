import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';

import { ICoche } from 'app/shared/model/coche.model';
import { CocheService } from './coche.service';
import { CocheDeleteDialogComponent } from './coche-delete-dialog.component';
import { CocheUpdateOwnerDialogComponent } from './coche-update-owner-dialog.component';

@Component({
  selector: 'jhi-coche',
  templateUrl: './coche.component.html'
})
export class CocheComponent implements OnInit, OnDestroy {
  coches: ICoche[] = [];
  cocheUni?: any;
  eventSubscriber?: Subscription;
  indice: any;
  isSaving = false;
  buscar: any;
  buscar2: any;
  typeSearch: any;
  activateByPrecio: Boolean = false;
  vendidoActive: Boolean = false;

  constructor(protected cocheService: CocheService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.coches = [];
    this.cocheService.query().subscribe((res: HttpResponse<ICoche[]>) => {
      this.coches = res.body ? res.body : [];
    });
  }

  loadActive(): void {
    this.coches = [];
    this.cocheService.findAllCochesActives().subscribe((r: HttpResponse<ICoche[]>) => {
      this.coches = r.body ? r.body : [];
    });
  }

  buscarByPrecio(): void {
    this.cocheService.findByPrecio(this.buscar, this.buscar2).subscribe((response: HttpResponse<ICoche[]>) => {
      this.coches = response.body ? response.body : [];
    });
  }

  buscarByPropietario(): void {
    this.cocheService.findByOwner(this.buscar).subscribe((res: HttpResponse<ICoche[]>) => {
      if (res.body) {
        this.coches = res.body ? res.body : [];
      }
    });
  }

  buscarByMarca(): void {
    this.cocheService.findByMarca(this.buscar).subscribe((res: HttpResponse<ICoche[]>) => {
      if (res.body) {
        this.coches = res.body ? res.body : [];
      }
    });
  }

  ngOnInit(): void {
    this.loadActive();
    this.registerChangeInCoches();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICoche): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCoches(): void {
    this.eventSubscriber = this.eventManager.subscribe('cocheListModification', () => this.loadAll());
  }

  delete(coche: ICoche): void {
    const modalRef = this.modalService.open(CocheDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.coche = coche;
  }

  subscribeToSaveResponse(result: Observable<HttpResponse<ICoche>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  previousState(): void {
    window.history.back();
  }

  onSaveSuccess(): void {
    this.isSaving = false;
  }

  onSaveError(): void {
    this.isSaving = false;
  }

  cambiarIndice(coche: ICoche): void {
    const modalRef = this.modalService.open(CocheUpdateOwnerDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.coche = coche;
  }
}
