import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'passPipe'
})
export class PassPipe implements PipeTransform {
  transform(value: string, activar: boolean): string {
    if (activar) {
      let salida = '';
      for (let i = 0; i < value.length; i++) {
        salida += '*';
      }
      return salida;
    } else {
      return value;
    }
  }
}
