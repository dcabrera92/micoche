import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'potencial'
})
export class PotencialPipe implements PipeTransform {
  transform(value: number, args: number): any {
    return Math.pow(value, args);
  }
}
