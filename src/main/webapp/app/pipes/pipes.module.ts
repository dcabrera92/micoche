import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PotencialPipe } from './potencial.pipe';
import { RemoveSpacesPipe } from './remove-spaces.pipe';
import { PassPipe } from './pass-directive.pipe';

@NgModule({
  declarations: [PotencialPipe, RemoveSpacesPipe, PassPipe],
  imports: [CommonModule],
  exports: [PotencialPipe, RemoveSpacesPipe, PassPipe]
})
export class PipesModule {}
