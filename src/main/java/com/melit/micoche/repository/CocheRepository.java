package com.melit.micoche.repository;

import com.melit.micoche.domain.Coche;
import com.melit.micoche.domain.auxiliar.VentaCoche;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Coche entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CocheRepository extends JpaRepository<Coche, Long>, JpaSpecificationExecutor<Coche>{

	List<Coche> findByMarca(String marca);
	
	
	@Query("select c from Coche c where c.propietario.nombre = :nombrePropietario ")
	List<Coche> findByNombrePropietario(@Param("nombrePropietario") String nombrePropietario);
	
	@Query( "select new com.melit.micoche.domain.auxiliar.VentaCoche(c.marca, count(c)) from Coche c where c.vendido = true group by c.marca order by count(c) desc" )
	List<VentaCoche> findMostSold();

	
	List<Coche> findByPrecioBetween(Integer desde, Integer hasta);


	List<Coche> findByVendidoFalse();

}
