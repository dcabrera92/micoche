package com.melit.micoche.repository.specification;

import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.melit.micoche.domain.Coche;
import com.melit.micoche.domain.Propietario;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

public interface CocheSpecification extends JpaSpecificationExecutor<Coche> {

    public static Specification<Coche> searchingParam(String filtro) {
        return new Specification<Coche> () {
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public Predicate toPredicate(Root<Coche> coche, CriteriaQuery<?> query, CriteriaBuilder builder) {

                //OR for the query's
                List<Predicate> ors = new ArrayList<Predicate>();
                Expression<String> marca = coche.get("marca").as(String.class);

                Join<Coche, Propietario> propietarios = coche.join("propietario", JoinType.LEFT);
                Expression<String> nombrePropietario = propietarios.get("nombre").as(String.class);
                Expression<String> apellidoPropietario = propietarios.get("apellido").as(String.class);

                String[] searchsParam = filtro.split(" ");

                for (int i = 0; i < searchsParam.length; i++){
                    List<Predicate> predicates = new ArrayList<Predicate>();
                    predicates.add(builder.like(builder.lower(marca), "%" + searchsParam[i].toLowerCase() + "%"));
                    predicates.add(builder.like(builder.lower(nombrePropietario), "%" + searchsParam[i].toLowerCase() + "%"));
                    predicates.add(builder.like(builder.lower(apellidoPropietario), "%" + searchsParam[i].toLowerCase() + "%"));
                
                    ors.add(builder.or(predicates.toArray(new Predicate[] {})));
                }

                Predicate result = builder.and(ors.toArray(new Predicate[] {}));
                query.distinct(true);
                return result;
            }

        };
    }

    public static Specification<Coche> searchingParams(String filtro, Float filtroPrecio) {
        return new Specification<Coche> () {
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public Predicate toPredicate(Root<Coche> coche, CriteriaQuery<?> query, CriteriaBuilder builder) {

                //OR for the query's
                List<Predicate> ors = new ArrayList<Predicate>();
                Expression<String> marca = coche.get("marca").as(String.class);
                Expression<Float> precio = coche.get("precio").as(Float.class);

                Join<Coche, Propietario> propietarios = coche.join("propietario", JoinType.LEFT);
                Expression<String> nombrePropietario = propietarios.get("nombre").as(String.class);
                Expression<String> apellidoPropietario = propietarios.get("apellido").as(String.class);

                String[] searchsParam = filtro.split(" ");

                for (int i = 0; i < searchsParam.length; i++){
                    List<Predicate> predicates = new ArrayList<Predicate>();
                    predicates.add(builder.like(builder.lower(marca), "%" + searchsParam[i].toLowerCase() + "%"));
                    predicates.add(builder.like(builder.lower(nombrePropietario), "%" + searchsParam[i].toLowerCase() + "%"));
                    predicates.add(builder.like(builder.lower(apellidoPropietario), "%" + searchsParam[i].toLowerCase() + "%"));
                
                    ors.add(builder.or(predicates.toArray(new Predicate[] {})));
                }
            
                Predicate result = builder.and(ors.toArray(new Predicate[] {}));
                query.distinct(true);
                
                Predicate finalResult = builder.and(result, builder.greaterThanOrEqualTo(precio, filtroPrecio));
                
                return finalResult;
            }

        };
    }
    //https://attacomsian.com/blog/spring-data-jpa-specifications
    //https://riptutorial.com/es/java/topic/840/json-en-java

    public static Specification<Coche> searchingParamsObject(JSONObject filtroJson) {
        return new Specification<Coche> () {
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public Predicate toPredicate(Root<Coche> coche, CriteriaQuery<?> query, CriteriaBuilder builder) {

				
				/*String filtroNombre = filtroJson.get("nombre", String.class);
				String filtroApellido = filtroJson.get("apelldio", String.class);
				String filroMarca = filtroJson.get("marca", String.class);
				Float filtroPrecio = filtroJson.get("precio", Float.class);*/
                //OR for the query's
                List<Predicate> ors = new ArrayList<Predicate>();
                Expression<String> marca = coche.get("marca").as(String.class);
                Expression<Float> precio = coche.get("precio").as(Float.class);

                Join<Coche, Propietario> propietarios = coche.join("propietario", JoinType.LEFT);
                Expression<String> nombrePropietario = propietarios.get("nombre").as(String.class);
                Expression<String> apellidoPropietario = propietarios.get("apellido").as(String.class);

                //String[] searchsParam = filtro.split(" ");

                for (int i = 0; i < searchsParam.length; i++){
                    List<Predicate> predicates = new ArrayList<Predicate>();
                    predicates.add(builder.like(builder.lower(marca), "%" + searchsParam[i].toLowerCase() + "%"));
                    predicates.add(builder.like(builder.lower(nombrePropietario), "%" + searchsParam[i].toLowerCase() + "%"));
                    predicates.add(builder.like(builder.lower(apellidoPropietario), "%" + searchsParam[i].toLowerCase() + "%"));
                
                    ors.add(builder.or(predicates.toArray(new Predicate[] {})));
                }
            
                Predicate result = builder.and(ors.toArray(new Predicate[] {}));
                query.distinct(true);
                
                Predicate finalResult = builder.and(result, builder.greaterThanOrEqualTo(precio, filtroPrecio));
                
                return finalResult;
            }

        };
    }
}