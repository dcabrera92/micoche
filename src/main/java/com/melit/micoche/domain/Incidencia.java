package com.melit.micoche.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Incidencia.
 */
@Entity
@Table(name = "incidencia")
public class Incidencia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "detalle", nullable = false)
    private String detalle;

    @NotNull
    @Column(name = "estado", nullable = false)
    private Boolean estado;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties("incidencias")
    private Coche coche;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetalle() {
        return detalle;
    }

    public Incidencia detalle(String detalle) {
        this.detalle = detalle;
        return this;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Boolean isEstado() {
        return estado;
    }

    public Incidencia estado(Boolean estado) {
        this.estado = estado;
        return this;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Coche getCoche() {
        return coche;
    }

    public Incidencia coche(Coche coche) {
        this.coche = coche;
        return this;
    }

    public void setCoche(Coche coche) {
        this.coche = coche;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Incidencia)) {
            return false;
        }
        return id != null && id.equals(((Incidencia) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Incidencia{" +
            "id=" + getId() +
            ", detalle='" + getDetalle() + "'" +
            ", estado='" + isEstado() + "'" +
            "}";
    }
}
