package com.melit.micoche.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * A Coche.
 */
@Entity
@Table(name = "coche")
public class Coche implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "marca", length = 20, nullable = false)
    private String marca;

    @NotNull
    @Size(max = 20)
    @Column(name = "modelo", length = 20, nullable = false)
    private String modelo;

    @NotNull
    @Column(name = "precio", nullable = false)
    private Integer precio;

    @Column(name = "vendido", nullable = true)
    private Boolean vendido;

    @NotNull
    @Size(max = 15)
    @Column(name = "matricula", length = 15, nullable = false)
    private String matricula;
    
    @Column(name="datesold")
    private Date datesold;

    @Column(name="color")
    private String color;

    public Date getDatesold() {
		return datesold;
	}

	public void setDatesold(Date datesold) {
		this.datesold = datesold;
	}
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonIgnoreProperties("coche")
    private Propietario propietario;
	
	@JsonIgnoreProperties("coche")
	@OneToMany(mappedBy = "coche", fetch = FetchType.EAGER)
    private Set<Incidencia> incidencias = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public Coche marca(String marca) {
        this.marca = marca;
        return this;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public Coche modelo(String modelo) {
        this.modelo = modelo;
        return this;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getPrecio() {
        return precio;
    }

    public Coche precio(Integer precio) {
        this.precio = precio;
        return this;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public Boolean isVendido() {
        return vendido;
    }

    public Coche vendido(Boolean vendido) {
        this.vendido = vendido;
        return this;
    }

    public void setVendido(Boolean vendido) {
        this.vendido = vendido;
    }

    public String getMatricula() {
        return matricula;
    }

    public Coche matricula(String matricula) {
        this.matricula = matricula;
        return this;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Coche color(String color) {
        this.color = color;
        return this;
    }
    
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}


    public Set<Incidencia> getIncidencias() {
        return incidencias;
    }

    public Coche incidencias(Set<Incidencia> incidencias) {
        this.incidencias = incidencias;
        return this;
    }

    public Coche addIncidencia(Incidencia incidencia) {
        this.incidencias.add(incidencia);
        incidencia.setCoche(this);
        return this;
    }

    public Coche removeIncidencia(Incidencia incidencia) {
        this.incidencias.remove(incidencia);
        incidencia.setCoche(null);
        return this;
    }

    public void setIncidencias(Set<Incidencia> incidencias) {
        this.incidencias = incidencias;
    }
    
    public Propietario getPropietario() {
        return propietario;
    }

    public Coche propietario(Propietario propietario) {
        this.propietario = propietario;
        return this;
    }

    public void setPropietario(Propietario propietario) {
        this.propietario = propietario;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Coche)) {
            return false;
        }
        return id != null && id.equals(((Coche) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Coche{" +
            "id=" + getId() +
            ", marca='" + getMarca() + "'" +
            ", modelo='" + getModelo() + "'" +
            ", precio=" + getPrecio() +
            ", vendido='" + isVendido() + "'" +
            ", matricula='" + getMatricula() + "'" +
            "}";
    }
}
