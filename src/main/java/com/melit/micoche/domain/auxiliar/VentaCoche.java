package com.melit.micoche.domain.auxiliar;
import java.io.Serializable;

/**
 * A Incidencia.
 */

public class VentaCoche implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public VentaCoche(String marca, long vendidos) {
		this.marca = marca;
		this.vendidos = vendidos;
	}

	private String marca;

    private long vendidos;
    
    
	/**
	 * @return the marca
	 */
	public String getMarca() {
		return marca;
	}

	/**
	 * @param marca the marca to set
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}

	/**
	 * @return the vendidos
	 */
	public long getVendidos() {
		return vendidos;
	}

	/**
	 * @param vendidos the vendidos to set
	 */
	public void setVendidos(long vendidos) {
		this.vendidos = vendidos;
	}
	
	@Override
    public String toString() {
        return "Coche{" +
            ", marca='" + getMarca() + "'" +
            ", vendido='" + getVendidos() + "'" +
            "}";
    }



}
